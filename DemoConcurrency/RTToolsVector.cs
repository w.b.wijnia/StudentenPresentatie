﻿using System;
using System.Numerics;

partial class RTTools
{
    private static Vector<float> _piVector, _brightnessVector, _epsilonVector;

    /// <summary>
    /// Initialises the Vector units used within the RTTools.
    /// </summary>
    static public void InitVector()
    {
        _piVector = new Vector<float>(PI);
        _brightnessVector = new Vector<float>(BRIGHTNESS);
        _epsilonVector = new Vector<float>(EPSILON);
    }

    /// <summary>
    /// Calculates the diffuse reflection for Vector units.
    /// </summary>
    static public void DiffuseReflectionVector(ref Vector<float> rX, ref Vector<float> rY, ref Vector<float> rZ, Vector<float> nX, Vector<float> nY, Vector<float> nZ, Random rng)
    {

        // See method DiffuseReflection for better understanding what is happening down here.

        /*
            float r1 = (float)rng.NextDouble();
            float r2 = (float)rng.NextDouble();
        */
        Vector<float> r1 = VectorHelp.GenerateRandomVector(rng);
        Vector<float> r2 = VectorHelp.GenerateRandomVector(rng);

        //   float r = (float)Math.Sqrt(1.0 - r1 * r1);
        Vector<float> r = Vector.SquareRoot(Vector<float>.One - r1 * r1);

        //   float phi = 2 * PI * r2;
        Vector<float> phi = Vector.Multiply(Vector.Multiply(_piVector, 2), r2);

        /*
            Vector3 R;
            R.X = (float)Math.Cos(phi) * r;
            R.Y = (float)Math.Sin(phi) * r;
            R.Z = r1;
        */
        float[] rXfloat = new float[instructionSet], rYfloat = new float[instructionSet], rZfloat = new float[instructionSet];
        for (int j = 0; j < instructionSet; j++)
        {
            rXfloat[j] = (float)Math.Cos(phi[j]) * r[j];
            rYfloat[j] = (float)Math.Sin(phi[j]) * r[j];
            rZfloat[j] = r1[j];
        }

        Vector<float> rXlocal = new Vector<float>(rXfloat);
        Vector<float> rYlocal = new Vector<float>(rYfloat);
        Vector<float> rZlocal = new Vector<float>(rZfloat);

        //  Vector3.Dot(N, R)
        Vector<float> dot = Vector.Add(
            Vector.Add(Vector.Multiply(nX, rXlocal), Vector.Multiply(nY, rYlocal)),
            Vector.Multiply(nZ, rZlocal));


        // if (Vector3.Dot(N, R) < 0) R *= -1.0f;
        Vector<int> mask = new Vector<int>();
        Vector.LessThan(dot, Vector<float>.Zero);

        rX = Vector.ConditionalSelect(mask, rXlocal, Vector.Multiply(rXlocal, -1));
        rY = Vector.ConditionalSelect(mask, rYlocal, Vector.Multiply(rYlocal, -1));
        rZ = Vector.ConditionalSelect(mask, rZlocal, Vector.Multiply(rZlocal, -1));

        return;
    }

    /// <summary>
    /// Calculates the refraction vector units.
    /// </summary>
    static public void RefractionVector(ref Vector<float> Rx, ref Vector<float> Ry, ref Vector<float> Rz, Ray4 ray, Random rng)
    {
        //  nc = inside ? 1 : 1.2f
        Vector<float> nc = Vector.ConditionalSelect(ray.inside, new Vector<float>(1f), new Vector<float>(1.2f));
        //  nt = inside ? 1.2f : 1
        Vector<float> nt = Vector.ConditionalSelect(ray.inside, new Vector<float>(1.2f), new Vector<float>(1f));
        //  nnt = nt / nc
        Vector<float> nnt = Vector.Divide(nt, nc);

        //  ddn = Vector3.Dot(D, N)
        Vector<float> ddn = VectorHelp.DotVector(ray.Dx4, ray.Dy4, ray.Dz4, ray.Nx4, ray.Ny4, ray.Nz4);

        //  float cos2t = 1.0f - nnt * nnt * (1 - ddn * ddn);
        Vector<float> cos2T = Vector.Subtract(Vector<float>.One, Vector.Multiply(Vector.Multiply(nnt, nnt), Vector.Subtract(Vector<float>.One, Vector.Multiply(ddn, ddn))));

        // r = d−2(d⋅n)n ( R = Vector3.Reflect(D, N) )

        VectorHelp.ReflectVector(ref Rx, ref Ry, ref Rz, ray.Dx4, ray.Dy4, ray.Dz4, ray.Nx4, ray.Ny4, ray.Nz4);

        // if (cos2t >= 0)
        Vector<int> mask = Vector.GreaterThanOrEqual(cos2T, Vector<float>.Zero);

        // Checks whether or not we actually need to continue execution.
        int cc = 0;
        for (int j = 0; j < instructionSet; j++)
            cc += mask[0];

        if (cc != 0)
        {
            Vector<float> r1 = VectorHelp.GenerateRandomVector(rng),
            a = Vector.Subtract(nt, nc),
            b = Vector.Add(nt, nc),
            r0 = Vector.Divide(Vector.Multiply(a, a), Vector.Multiply(b, b)),
            c = Vector.Add(Vector<float>.One, ddn),

            // float Tr = 1 - (R0 + (1 - R0) * c * c * c * c * c);
            Tr = Vector.Subtract(Vector<float>.One, Vector.Add(r0, Vector.Multiply(Vector.Subtract(Vector<float>.One, r0), (c * c * c * c * c)))),

            constant = Vector.Add(Vector.Multiply(ddn, nnt), Vector.SquareRoot(cos2T));

            // if (r1 < Tr)
            mask = Vector.LessThan(r1, Tr);

            // R = (D * nnt - N * (ddn * nnt + (float)Math.Sqrt(cos2t)));

            Rx = Vector.ConditionalSelect(mask, Rx,
                Vector.Subtract(Vector.Multiply(ray.Dx4, nnt), Vector.Multiply(ray.Nx4, constant)));

            Ry = Vector.ConditionalSelect(mask, Ry,
                Vector.Subtract(Vector.Multiply(ray.Dy4, nnt), Vector.Multiply(ray.Ny4, constant)));

            Rz = Vector.ConditionalSelect(mask, Rz,
                Vector.Subtract(Vector.Multiply(ray.Dz4, nnt), Vector.Multiply(ray.Nz4, constant)));
        }
    }
    
    /// <summary>
    /// Calculates the integer RGB value for vector units.
    /// </summary>
    /// <returns></returns>
    static public Vector<int> Vector3ToIntegerRGBVector(Vector<float> r, Vector<float> g, Vector<float> b)
    {
        int[] i = new int[instructionSet];

        for (int j = 0; j < instructionSet; j++)
        {
            i[j] =
                ((int)Math.Min(255, 256.0f * BRIGHTNESS * Math.Sqrt(r[j])) << 16) +
                ((int)Math.Min(255, 256.0f * BRIGHTNESS * Math.Sqrt(g[j])) << 8) +
                (int)Math.Min(255, 256.0f * BRIGHTNESS * Math.Sqrt(b[j]));
        }
        return new Vector<int>(i);
    }

}

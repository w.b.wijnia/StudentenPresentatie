﻿//#define DEBUGCAMERA

using System;
using System.Numerics;

using Template;

partial class Game
{
    private Vector<float> PIVector = new Vector<float>(PI);
    private Vector<float> INVPIVector = new Vector<float>(INVPI);
    private Vector<float> EPSILONVector = new Vector<float>(EPSILON);

    /// <summary>
    /// Samples the scene for a ray struct.
    /// </summary>
    private void SampleVector(ref Vector<float> r, ref Vector<float> g, ref Vector<float> b, Ray4 ray, int depth, int inThread)
    {
        // Terminate when the path is simply too long.
        if (depth > MAXDEPTH)
            return;

        // DEBUGGING
#if (DEBUGCAMERA)
            r = ray.Dx4;
            g = ray.Dy4;
            b = ray.Dz4;

            return;
#endif
        // DEBUGGING

        // local values required for addition at some points.
        Vector<float> rLocal = new Vector<float>(0), gLocal = new Vector<float>(0), bLocal = new Vector<float>(0);
        Vector<int> maskOdd = new Vector<int>(), maskEven = new Vector<int>();
        int[] t;

        // Scene.Intersect(ray);
        //Scene.IntersectVector(ray);

        /*
        if (ray.objIdx == -1)
        {
            // no scene primitive encountered; skybox
            return 1.0f * scene.SampleSkydome(ray.D);
        }
        */
        maskOdd = Vector.Equals(ray.objIdx, new Vector<int>(-1));

        scene.SampleSkyDomeVector(ref rLocal, ref gLocal, ref bLocal, ray.Dx4, ray.Dy4, ray.Dz4);

        r = Vector.ConditionalSelect(maskOdd, 0.50f * rLocal + r, r);
        g = Vector.ConditionalSelect(maskOdd, 0.50f * gLocal + g, g);
        b = Vector.ConditionalSelect(maskOdd, 0.50f * bLocal + b, b);

        return;

        if (Vector.EqualsAll(maskOdd, VectorHelp.MaskTrue))
            return;

        //Vector3 I = ray.O + ray.t * ray.D;
        Vector<float> Ix = Vector.Add(ray.Ox4, Vector.Multiply(ray.t4, ray.Dx4));
        Vector<float> Iy = Vector.Add(ray.Oy4, Vector.Multiply(ray.t4, ray.Dy4));
        Vector<float> Iz = Vector.Add(ray.Oz4, Vector.Multiply(ray.t4, ray.Dz4));

        // Material material = scene.GetMaterial(ray.objIdx, I);
        MaterialVector mat = scene.GetMaterialVector(ray.objIdx, Ix, Iy, Iz);

        /*
        if (material.emissive)
        {
            // hit light
            return material.diffuse;
        }
        */
        maskEven = mat.emissive;

        r = Vector.ConditionalSelect(maskEven, Vector.Add(r, mat.diffuseX), r);
        g = Vector.ConditionalSelect(maskEven, Vector.Add(g, mat.diffuseY), g);
        b = Vector.ConditionalSelect(maskEven, Vector.Add(b, mat.diffuseZ), b);

        // float r0 = (float)r[inThread].NextDouble();
        Vector<float> r0 = VectorHelp.GenerateRandomVector(this.rng[inThread]);

        // Vector3 R = Vector3.Zero;
        Vector<float> Rx = new Vector<float>(0);
        Vector<float> Ry = new Vector<float>(0);
        Vector<float> Rz = new Vector<float>(0);

        maskEven = Vector.BitwiseOr(maskOdd, maskEven);
        if (Vector.EqualsAll(maskEven, VectorHelp.MaskTrue))
            return;

        /*
        if (r0 < material.refr)
        {
            // dielectric: refract or reflect
            RTTools.Refraction(ray.inside, ray.D, ray.N, ref R, r[inThread]);
            Ray extensionRay = new Ray(I + R * EPSILON, R, 1e34f);
            extensionRay.inside = (Vector3.Dot(ray.N, R) < 0);
            return material.diffuse * Sample(extensionRay, depth + 1, inThread);
        }
        */
        maskOdd = Vector.LessThan(r0, mat.refr);
        RTTools.RefractionVector(ref Rx, ref Ry, ref Rz, ray, this.rng[inThread]);

        t = VectorHelp.GetMaskTrueValues(maskOdd);
        for (int j = 0, l = t.Length; j < l; j++)
        {
            int i = t[j];
            Ray4 exRay = new Ray4();

            exRay.Ox4 = Vector.Add(new Vector<float>(Ix[i]), Vector.Multiply(new Vector<float>(Rx[i]), EPSILONVector));
            exRay.Oy4 = Vector.Add(new Vector<float>(Iy[i]), Vector.Multiply(new Vector<float>(Ry[i]), EPSILONVector));
            exRay.Oz4 = Vector.Add(new Vector<float>(Iz[i]), Vector.Multiply(new Vector<float>(Rz[i]), EPSILONVector));

            exRay.Dx4 = Vector.Multiply(new Vector<float>(Rx[i]), VectorHelp.GenerateSmallDivergenceVector(this.rng[inThread]));
            exRay.Dy4 = Vector.Multiply(new Vector<float>(Ry[i]), VectorHelp.GenerateSmallDivergenceVector(this.rng[inThread]));
            exRay.Dz4 = Vector.Multiply(new Vector<float>(Rz[i]), VectorHelp.GenerateSmallDivergenceVector(this.rng[inThread]));

            exRay.inside = new Vector<int>(Convert.ToInt32(Vector.Dot(new Vector<float>(new float[4] { Rx[i], Ry[i], Rz[i], 0 }), new Vector<float>(new float[4] { ray.Nx4[i], ray.Nx4[i], ray.Nx4[i], 0 })) < 0));

            exRay.objIdx = new Vector<int>(-1);
            exRay.t4 = new Vector<float>(1e34f);

            rLocal = new Vector<float>(0);
            gLocal = new Vector<float>(0);
            bLocal = new Vector<float>(0);

            SampleVector(ref rLocal, ref gLocal, ref bLocal, exRay, depth + 1, inThread);

            float[] fr = new float[4], fg = new float[4], fb = new float[4];
            fr[i] = VectorHelp.GetTotalFloatVector(rLocal) * mat.diffuseX[i] * 0.25f;
            fg[i] = VectorHelp.GetTotalFloatVector(gLocal) * mat.diffuseY[i] * 0.25f;
            fb[i] = VectorHelp.GetTotalFloatVector(bLocal) * mat.diffuseZ[i] * 0.25f;

            r = Vector.Add(new Vector<float>(fr), r);
            g = Vector.Add(new Vector<float>(fg), g);
            b = Vector.Add(new Vector<float>(fb), b);
        }

        maskOdd = Vector.BitwiseOr(maskOdd, maskEven);
        if (Vector.EqualsAll(maskOdd, VectorHelp.MaskTrue))
            return;

        /*
        else if ((r0 < (material.refl + material.refr)))
        {
            // pure specular reflection
            R = Vector3.Reflect(ray.D, ray.N);
            Ray extensionRay = new Ray(I + R * EPSILON, R, 1e34f);
            return material.diffuse * Sample(extensionRay, depth + 1, inThread);
        }
        */
        maskEven = Vector.LessThan(r0, Vector.Add(mat.refl, mat.refr));

        t = VectorHelp.GetMaskTrueValues(maskEven);
        for (int j = 0, l = t.Length; j < l; j++)
        {
            int i = t[j];
            Ray4 exRay = new Ray4();

            Vector3 Ref = Vector3.Reflect(new Vector3(ray.Dx4[i], ray.Dy4[i], ray.Dz4[i]), new Vector3(ray.Nx4[i], ray.Ny4[i], ray.Nz4[i]));

            exRay.Ox4 = Vector.Add(new Vector<float>(Ix[i]), Vector.Multiply(new Vector<float>(Ref.X), EPSILONVector));
            exRay.Oy4 = Vector.Add(new Vector<float>(Iy[i]), Vector.Multiply(new Vector<float>(Ref.Y), EPSILONVector));
            exRay.Oz4 = Vector.Add(new Vector<float>(Iz[i]), Vector.Multiply(new Vector<float>(Ref.Z), EPSILONVector));

            exRay.Dx4 = Vector.Multiply(new Vector<float>(Ref.X), VectorHelp.GenerateSmallDivergenceVector(this.rng[inThread]));
            exRay.Dy4 = Vector.Multiply(new Vector<float>(Ref.Y), VectorHelp.GenerateSmallDivergenceVector(this.rng[inThread]));
            exRay.Dz4 = Vector.Multiply(new Vector<float>(Ref.Z), VectorHelp.GenerateSmallDivergenceVector(this.rng[inThread]));

            exRay.inside = new Vector<int>(0);
            exRay.objIdx = new Vector<int>(-1);
            exRay.t4 = new Vector<float>(1e34f);

            rLocal = new Vector<float>(0);
            gLocal = new Vector<float>(0);
            bLocal = new Vector<float>(0);

            SampleVector(ref rLocal, ref gLocal, ref bLocal, exRay, depth + 1, inThread);

            float[] fr = new float[4], fg = new float[4], fb = new float[4];
            fr[i] = VectorHelp.GetTotalFloatVector(rLocal) * mat.diffuseX[i] * 0.25f;
            fg[i] = VectorHelp.GetTotalFloatVector(gLocal) * mat.diffuseY[i] * 0.25f;
            fb[i] = VectorHelp.GetTotalFloatVector(bLocal) * mat.diffuseZ[i] * 0.25f;

            r = Vector.Add(new Vector<float>(fr), r);
            g = Vector.Add(new Vector<float>(fg), g);
            b = Vector.Add(new Vector<float>(fb), b);
        }

        maskEven = Vector.BitwiseOr(maskOdd, maskEven);
        if (Vector.EqualsAll(maskEven, VectorHelp.Mask1))
            return;

        /*
        else
        {
            // diffuse reflection
            R = RTTools.DiffuseReflection(ray.N, r[inThread]);
            Ray extensionRay = new Ray(I + R * EPSILON, R, 1e34f);
            return Vector3.Dot(R, ray.N) * material.diffuse * Sample(extensionRay, depth + 1, inThread);
        }
        */
        maskOdd = Vector.LessThan(r0, Vector.Add(mat.refl, mat.refr));

        RTTools.DiffuseReflectionVector(ref Rx, ref Ry, ref Rz, ray.Nx4, ray.Ny4, ray.Nz4, this.rng[inThread]);

        t = VectorHelp.GetMaskTrueValues(maskEven);
        for (int j = 0, l = t.Length; j < l; j++)
        {
            int i = t[j];
            Ray4 exRay = new Ray4();

            Vector3 Ref = Vector3.Reflect(new Vector3(ray.Dx4[i], ray.Dy4[i], ray.Dz4[i]), new Vector3(ray.Nx4[i], ray.Ny4[i], ray.Nz4[i]));

            exRay.Ox4 = Vector.Add(new Vector<float>(Ix[i]), Vector.Multiply(new Vector<float>(Rx[i]), EPSILONVector));
            exRay.Oy4 = Vector.Add(new Vector<float>(Iy[i]), Vector.Multiply(new Vector<float>(Ry[i]), EPSILONVector));
            exRay.Oz4 = Vector.Add(new Vector<float>(Iz[i]), Vector.Multiply(new Vector<float>(Rz[i]), EPSILONVector));

            exRay.Dx4 = Vector.Multiply(new Vector<float>(Rx[i]), VectorHelp.GenerateSmallDivergenceVector(this.rng[inThread]));
            exRay.Dy4 = Vector.Multiply(new Vector<float>(Ry[i]), VectorHelp.GenerateSmallDivergenceVector(this.rng[inThread]));
            exRay.Dz4 = Vector.Multiply(new Vector<float>(Rz[i]), VectorHelp.GenerateSmallDivergenceVector(this.rng[inThread]));

            exRay.inside = new Vector<int>(0);
            exRay.objIdx = new Vector<int>(-1);
            exRay.t4 = new Vector<float>(1e34f);

            rLocal = new Vector<float>(0);
            gLocal = new Vector<float>(0);
            bLocal = new Vector<float>(0);

            SampleVector(ref rLocal, ref gLocal, ref bLocal, exRay, depth + 1, inThread);

            float[] fr = new float[4], fg = new float[4], fb = new float[4];
            fr[i] = VectorHelp.GetTotalFloatVector(rLocal) * mat.diffuseX[i] * 0.25f;
            fg[i] = VectorHelp.GetTotalFloatVector(gLocal) * mat.diffuseY[i] * 0.25f;
            fb[i] = VectorHelp.GetTotalFloatVector(bLocal) * mat.diffuseZ[i] * 0.25f;

            r = Vector.Add(new Vector<float>(fr), r);
            g = Vector.Add(new Vector<float>(fg), g);
            b = Vector.Add(new Vector<float>(fb), b);
        }
    }
}
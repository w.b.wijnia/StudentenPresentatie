﻿//#define DEBUGCAMERA
//#define DEBUGGINGTHREADS

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Numerics;
using System.Threading.Tasks;

using System.Threading;

using Template;



// making vectors work in VS2013:
// - Uninstall Nuget package manager
// - Install Nuget v2.8.6 or later
// - In Package Manager Console: Install-Package System.Numerics.Vectors -Pre

partial class Game
{
    // member variables
    public Surface screen;                  // target canvas
    Camera camera;                          // camera
    Scene scene;                            // hardcoded scene
    Stopwatch timer = new Stopwatch();      // timer
    Vector3[] accumulator;                  // buffer for accumulated samples
    int spp = 0;                            // samples per pixel; accumulator will be divided by this
    int runningTime = -1;                   // running time (from commandline); default = -1 (infinite)
    bool useGPU = true;                     // GPU code enabled (from commandline)
    int gpuPlatform = 0;                    // OpenCL platform to use (from commandline)
    bool firstFrame = true;                 // first frame: used to start timer once
                                            // constants for rendering algorithm
    const float PI = 3.14159265359f;
    const float INVPI = 1.0f / PI;
    const float EPSILON = 0.0001f;
    const int MAXDEPTH = 20;


    // clear the accumulator: happens when camera moves
    private void ClearAccumulator()
    {
        for (int s = screen.width * screen.height, i = 0; i < s; i++)
            accumulator[i] = Vector3.Zero;
        spp = 0;
    }


    // initialize renderer: takes in command line parameters passed by template code
    public void Init(int rt, bool gpu, int platformIdx)
    {
        // pass command line parameters
        runningTime = rt;
        useGPU = gpu;
        gpuPlatform = platformIdx;
        // initialize accumulator
        accumulator = new Vector3[screen.width * screen.height];
        ClearAccumulator();
        // setup scene
        scene = new Scene();
        // setup camera
        camera = new Camera(screen.width, screen.height);

        InitialiseThreading();
    }
    // sample: samples a single path up to a maximum depth
    private Vector3 Sample(Ray ray, int depth, int inThread)
    {

#if (DEBUGCAMERA)
            return ray.D;
#endif

        // find nearest ray/scene intersection
        Scene.Intersect(ray);
        if (ray.objIdx == -1)
        {
            // no scene primitive encountered; skybox
            return 1.0f * scene.SampleSkydome(ray.D);
        }
        // calculate intersection point
        Vector3 I = ray.O + ray.t * ray.D;
        // get material at intersection point
        Material material = scene.GetMaterial(ray.objIdx, I);
        if (material.emissive)
        {
            // hit light
            return material.diffuse;
        }
        // terminate if path is too long
        if (depth >= MAXDEPTH) return Vector3.Zero;
        // handle material interaction
        float r0 = (float)rng[inThread].NextDouble();
        Vector3 R = Vector3.Zero;
        if (r0 < material.refr)
        {
            // dielectric: refract or reflect
            RTTools.Refraction(ray.inside, ray.D, ray.N, ref R, rng[inThread]);
            Ray extensionRay = new Ray(I + R * EPSILON, R, 1e34f);
            extensionRay.inside = (Vector3.Dot(ray.N, R) < 0);
            return material.diffuse * Sample(extensionRay, depth + 1, inThread);
        }
        else if ((r0 < (material.refl + material.refr)) && (depth < MAXDEPTH))
        {
            // pure specular reflection
            R = Vector3.Reflect(ray.D, ray.N);
            Ray extensionRay = new Ray(I + R * EPSILON, R, 1e34f);
            return material.diffuse * Sample(extensionRay, depth + 1, inThread);
        }
        else
        {
            // diffuse reflection
            R = RTTools.DiffuseReflection(ray.N, rng[inThread]);
            Ray extensionRay = new Ray(I + R * EPSILON, R, 1e34f);
            return Vector3.Dot(R, ray.N) * material.diffuse * Sample(extensionRay, depth + 1, inThread);
        }
    }

    private Vector3 SampleIterative(Ray ray, int depth, int inThread)
    {
        // The current ray.
        Ray r = ray;
        int d = depth;

        Vector3 color = Vector3.Zero;
        Vector3 colorMultiplier = Vector3.One;

        Vector3 I = Vector3.Zero;
        Vector3 R = Vector3.Zero;

        float r0 = 0;

        Material material = new Material();

#if (DEBUGCAMERA)
            return ray.D;
#endif

        while (true)
        {
            // Once we reach this depth, stop the iteration. No further rays are required.
            if (d > MAXDEPTH)
                break;

            // find nearest ray/scene intersection
            Scene.Intersect(r);

            if (r.objIdx == -1)
            {
            // no scene primitive encountered; skybox
                color += 1.0f * scene.SampleSkydome(r.D);
                // Stop the iteration, no further rays are required.
                break;
            }

            // calculate intersection point
            I = r.O + r.t * r.D;
            // get material at intersection point
            material = scene.GetMaterial(r.objIdx, I);

            // Hitting a light.
            if (material.emissive)
            {
                color += material.diffuse;
                // Stop the iteration, no further rays are required.
                break;
            }

            // handle material interaction
            r0 = (float)rng[inThread].NextDouble();
            R = Vector3.Zero;
            if (r0 < material.refr)
            {       // dielectric: refract or reflect
                colorMultiplier *= material.diffuse;
                RTTools.Refraction(r.inside, r.D, r.N, ref R, rng[inThread]);

                Ray exRay = new Ray(I + R * EPSILON, R, 1e34f);
                exRay.inside = (Vector3.Dot(r.N, R) < 0);
                
                r = exRay;
            }

            else if ((r0 < (material.refl + material.refr)))
            {       // pure specular reflection
                R = Vector3.Reflect(r.D, r.N);
                colorMultiplier *= material.diffuse;

                r = new Ray(I + R * EPSILON, R, 1e34f);
            }

            else
            {       // diffuse reflection
                R = RTTools.DiffuseReflection(r.N, rng[inThread]);
                colorMultiplier *= Vector3.Dot(R, r.N) * material.diffuse;
                
                r = new Ray(I + R * EPSILON, R, 1e34f);
            }

            // Increase the depth.
            d++;

        }

        //Console.WriteLine(color);

        return color *= colorMultiplier;

    }

    Stopwatch sRender = new Stopwatch(), sOther = new Stopwatch();
    FloatAverage fRender = new FloatAverage(60), fOther = new FloatAverage(60);

    // tick: renders one frame
    public void Tick()
    {
        sOther.Stop();
        sRender.Reset();
        sRender.Start();

        // initialize timer
        if (firstFrame)
        {
            timer.Reset();
            timer.Start();
            firstFrame = false;
        }
        // handle keys, only when running time set to -1 (infinite)
        if (runningTime == -1)
            if (camera.HandleInput())
            {
                // camera moved; restart
                ClearAccumulator();
            }
        // render
        if (false) // if (useGPU)
        {
            // add your CPU + OpenCL path here
            // mind the gpuPlatform parameter! This allows us to specify the platform on our
            // test system.
            // note: it is possible that the automated test tool provides you with a different
            // platform number than required by your hardware. In that case, you can hardcode
            // the platform during testing (ignoring gpuPlatform); do not forget to put back
            // gpuPlatform before submitting!
        }
        else
        {
#if(DEBUGGINGTHREADS)
            Console.WriteLine("Next iteration started!");
#endif
            RenderThreaded();
        }

        sRender.Stop();

        fRender.Add(sRender.ElapsedMilliseconds);
        fOther.Add(sOther.ElapsedMilliseconds);
        fTest.Add(sTest.ElapsedMilliseconds);
        //fPercentage.Add(fUpdate.Average() / (fUpdate.Average() + fRender.Average() + fOther.Average()));

        string tu = "update time: " + ((int)fRender.Recent()).ToString("000") + " (average: " + fRender.Average().ToString("000.00000") + ")";
        screen.Print(tu, 10, 10, 0x000000);
        string to = "other time:  " + ((int)fOther.Recent()).ToString("000") + " (average: " + fOther.Average().ToString("000.00000") + ")";
        screen.Print(to, 10, 30, 0x000000);
        string tt = "test time:   " + ((int)fTest.Recent()).ToString("000") + " (average: " + fTest.Average().ToString("000.00000") + ")";
        screen.Print(tt, 10, 50, 0x000000);

        // stop and report when max render time elapsed
        int elapsedSeconds = (int)(timer.ElapsedMilliseconds / 1000);
        if (runningTime != -1)
            if (elapsedSeconds >= runningTime)
            {
                OpenTKApp.Report((int)timer.ElapsedMilliseconds, spp, screen);
            }
        //OpenTKApp.Report((int)timer.ElapsedMilliseconds, spp, screen);
        sOther.Restart();
        sOther.Start();

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class Coordinates
{
    public int x, y;

    public Coordinates(int x, int y)
    {
        this.x = x;
        this.y = y;
    }
}

public class Stratification
{
    /// <summary>
    /// The number of X blocks.
    /// </summary>
    int x;
    /// <summary>
    /// The number of Y blocks.
    /// </summary>
    int y;

    /// <summary>
    /// Keeps track of where we are at every X when considering the Y.
    /// </summary>
    int[] tracker;

    /// <summary>
    /// Keeps track of where we are within the tracker.
    /// </summary>
    int internalTracker;


    public Stratification(int x, int y)
    {
        this.x = x;
        this.y = y;

        tracker = new int[x];

        internalTracker = 0;
    }

    /// <summary>
    /// Resets the stratification. Allowing it to start all over again.
    /// </summary>
    public void Reset()
    {
        for (int j = 0, l = x; j < l; j++)
            tracker[j] = 0;

        internalTracker = 0;
    }

    /// <summary>
    /// Retrieves a block.
    /// </summary>
    /// <returns>Returns a coordinate object that holds the X and Y.</returns>
    public Coordinates Get()
    {
        lock (this)
        {
            if (internalTracker >= x) return null;

            Coordinates c = new Coordinates(internalTracker, tracker[internalTracker]);
            tracker[internalTracker]++;

            if (tracker[internalTracker] > (y - 1))
                internalTracker++;

            return c;
        }
    }
}

﻿/// <summary>
/// Encapsulates taking the average of X floats.
/// </summary>
public class FloatAverage
{
    /// <summary>
    /// The floats to take the average from.
    /// </summary>
    float[] floats;

    /// <summary>
    /// The current average. Is set to float.MaxValue when something is added to the list.
    /// </summary>
    float average;

    public FloatAverage(int count)
    {
        floats = new float[count];
    }

    /// <summary>
    /// Adds a float to the list.
    /// </summary>
    /// <param name="f"></param>
    public void Add(float f)
    {
        for (int j = floats.Length - 1; j > 0; j--)
            floats[j] = floats[j - 1];

        floats[0] = f;
        average = float.MaxValue;
    }

    /// <summary>
    /// Calculates the average. Is only calculated once until a new addition has been done.
    /// </summary>
    /// <param name="count"></param>
    /// <returns></returns>
    public float Average(int count = -1)
    {
        if (average == float.MaxValue)
        {

            average = 0;
            count = count == -1 ? floats.Length : count;

            for (int j = 0, l = count; j < l; j++)
                average += floats[j];

            average /= (float)count;
        }

        return average;
    }

    /// <summary>
    /// Retrieves the most recent addition.
    /// </summary>
    /// <returns></returns>
    public float Recent()
    {
        return floats[0];
    }
}

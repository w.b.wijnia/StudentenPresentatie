﻿using System;
using System.Collections.Generic;
using System.Numerics;

public static class VectorHelp
{

    public const int instructionSet = 4;

    private static Vector<int> mask0 = new Vector<int>(0);
    private static Vector<int> mask1 = new Vector<int>(1);
    private static Vector<int> mask2 = new Vector<int>(2);
    private static Vector<int> mask3 = new Vector<int>(3);
    private static Vector<int> mask4 = new Vector<int>(4);
    private static Vector<int> mask5 = new Vector<int>(5);
    private static Vector<int> mask6 = new Vector<int>(6);
    private static Vector<int> mask7 = new Vector<int>(7);
    private static Vector<int> mask8 = new Vector<int>(7);

    private static Vector<int> maskTrue = Vector.LessThan(new Vector<float>(0), new Vector<float>(1));
    private static Vector<int> maskFalse = Vector.LessThan(new Vector<float>(1), new Vector<float>(0));

    static public float GetTotalFloatVector(Vector<float> v)
    {
        float f = 0;
        for (int j = 0; j < instructionSet; j++)
            f += v[j];
        return f;
    }

    static public void ReflectVector(ref Vector<float> Rx, ref Vector<float> Ry, ref Vector<float> Rz, Vector<float> Dx, Vector<float> Dy, Vector<float> Dz, Vector<float> Nx, Vector<float> Ny, Vector<float> Nz)
    {
        // r = d−2(d⋅n)n ( R = Vector3.Reflect(D, N) )

        // Normalisation of N.
        VectorHelp.NormalizeVector(ref Nx, ref Ny, ref Nz);

        // R = Vector3.Reflect(D, N);
        Rx = Vector.Subtract(Dx, Vector.Multiply(2, Vector.Multiply(Vector.Multiply(Dx, Nx), Nx)));
        Ry = Vector.Subtract(Dy, Vector.Multiply(2, Vector.Multiply(Vector.Multiply(Dy, Ny), Ny)));
        Rz = Vector.Subtract(Dz, Vector.Multiply(2, Vector.Multiply(Vector.Multiply(Dz, Nz), Nz)));
    }

    static public int[] GetMaskTrueValues(Vector<int> mask)
    {
        int[] i = new int[1];
        int c = 0;
        int m = 0;

        // 1 0 1 0 1
        for(int j = 0; j < instructionSet; j++)
        {
            c++;            // 1, 2, 3, 4, 5
            m += mask[j];   // 1, 1, 2, 2, 3

                             // 0, 1, 1, 2, 2
            int[] n = new int[i.Length + mask[j]]; // 1, 1, 2, 2, 3
            n[m] = c;
            i.CopyTo(n, 0);

            i = n;
        }
        return i;
    }

    /// <summary>
    /// Normalises the given vector unit.
    /// </summary>
    static public void NormalizeVector(ref Vector<float> x, ref Vector<float> y, ref Vector<float> z)
    {
        Vector<float> length = Vector.SquareRoot(VectorHelp.DotVector(x, y, z));

        x = Vector.Divide(x, length);
        y = Vector.Divide(y, length);
        z = Vector.Divide(z, length);
    }

    /// <summary>
    /// Calculates the dot product between two vector units.
    /// </summary>
    static public Vector<float> DotVector(Vector<float> x1, Vector<float> y1, Vector<float> z1, Vector<float> x2, Vector<float> y2, Vector<float> z2)
    {
        return Vector.Add(
                Vector.Add(Vector.Multiply(x1, x2),
                Vector.Multiply(y1, y2)),
                Vector.Multiply(z1, z2));
    }

    /// <summary>
    /// Calculates the dot product with the same vector unit.
    /// </summary>
    static public Vector<float> DotVector(Vector<float> x1, Vector<float> y1, Vector<float> z1)
    {
        return Vector.Add(
                Vector.Add(Vector.Multiply(x1, x1),
                Vector.Multiply(y1, y1)),
                Vector.Multiply(z1, z1));
    }

    /// <summary>
    /// Generates a vector unit filled with random values.
    /// </summary>
    static public Vector<float> GenerateRandomVector(Random rng)
    {
        float[] r = new float[instructionSet];

        for (int j = 0, l = instructionSet; j < l; j++)
            r[j] = (float)rng.NextDouble();

        return new Vector<float>(r);
    }

    /// <summary>
    /// Generates a vector unit filled with random values.
    /// The offset is added to the generated number.
    /// </summary>
    static public Vector<float> GenerateRandomVector(Random rng, float offset)
    {
        return Vector.Add(GenerateRandomVector(rng), new Vector<float>(offset));
    }

    /// <summary>
    /// Generates a vector that has values between 0.95 and 1.05.
    /// </summary>
    static public Vector<float> GenerateSmallDivergenceVector(Random rng)
    {
        return Vector.Add(Vector.Multiply(GenerateRandomVector(rng), 0.1f ), new Vector<float>(0.95f));
    }

    public static Vector<int> Mask0 { get { return mask0; } }
    public static Vector<int> Mask1 { get { return mask1; } }
    public static Vector<int> Mask2 { get { return mask2; } }
    public static Vector<int> Mask3 { get { return mask3; } }
    public static Vector<int> Mask4 { get { return mask4; } }
    public static Vector<int> Mask5 { get { return mask5; } }
    public static Vector<int> Mask6 { get { return mask6; } }
    public static Vector<int> Mask7 { get { return mask7; } }
    public static Vector<int> Mask8 { get { return mask8; } }

    public static Vector<int> MaskTrue {  get { return maskTrue; } }
    public static Vector<int> MaskFalse { get { return maskFalse; } }
}


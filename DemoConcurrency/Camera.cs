﻿using System;
using System.Numerics;

    class Camera
    {
        // Camera data

        public Vector3 pos;
        public Vector3 target;
        public float focalDistance;
        Vector3 up, right;

        public Vector3 E;
        Vector3 p1, p2, p3;
        int screenWidth, screenHeight;
        float aspectRatio, lensSize;

        // SIMD

        Vector<float> screenWidthVector, screenHeightVector;
        Vector<float> lensSizeVector;

        Vector<float> upX, upY, upZ;
        Vector<float> rightX, rightY, rightZ;
        Vector<float> posX, posY, posZ;

        Vector<float> p1X, p2p1X, p3p1X;
        Vector<float> p1Y, p2p1Y, p3p1Y;
        Vector<float> p1Z, p2p1Z, p3p1Z;

        public Camera(int w, int h)
        {
            screenWidth = w;
            screenHeight = h;
            aspectRatio = (float)w / (float)h;
            lensSize = 0.04f;
            lensSizeVector = new Vector<float>(lensSize);
            pos = new Vector3(-0.94f, -0.037f, -3.342f);
            target = new Vector3(-0.418f, -0.026f, -2.435f);

            screenWidthVector = new Vector<float>(w);
            screenHeightVector = new Vector<float>(h);

            Update();
        }
        public bool HandleInput()
        {
            var keyboard = OpenTK.Input.Keyboard.GetState();
            target = pos + E;
            bool changed = false;
            if (keyboard[OpenTK.Input.Key.A]) { changed = true; pos -= right * 0.1f; target -= right * 0.1f; }
            if (keyboard[OpenTK.Input.Key.D]) { changed = true; pos += right * 0.1f; target += right * 0.1f; }
            if (keyboard[OpenTK.Input.Key.W]) { changed = true; pos += E * 0.1f; }
            if (keyboard[OpenTK.Input.Key.S]) { changed = true; pos -= E * 0.1f; }
            if (keyboard[OpenTK.Input.Key.R]) { changed = true; pos += up * 0.1f; target += up * 0.1f; }
            if (keyboard[OpenTK.Input.Key.F]) { changed = true; pos -= up * 0.1f; target -= up * 0.1f; }
            if (keyboard[OpenTK.Input.Key.Up]) { changed = true; target -= up * 0.1f; }
            if (keyboard[OpenTK.Input.Key.Down]) { changed = true; target += up * 0.1f; }
            if (keyboard[OpenTK.Input.Key.Left]) { changed = true; target -= right * 0.1f; }
            if (keyboard[OpenTK.Input.Key.Right]) { changed = true; target += right * 0.1f; }
            if (changed)
            {
                Update();
                return true;
            }
            return false;
        }
        public void Update()
        {
            // construct a look-at matrix
            E = Vector3.Normalize(target - pos);
            up = Vector3.UnitY;
            right = Vector3.Cross(up, E);
            up = Vector3.Cross(E, right);
            // calculate focal distance
            Ray ray = new Ray(pos, E, 1e34f);
            Scene.Intersect(ray);
            focalDistance = Math.Min(20, ray.t);
            // calculate virtual screen corners
            Vector3 C = pos + focalDistance * E;
            p1 = C - 0.5f * focalDistance * aspectRatio * right + 0.5f * focalDistance * up;
            p2 = C + 0.5f * focalDistance * aspectRatio * right + 0.5f * focalDistance * up;
            p3 = C - 0.5f * focalDistance * aspectRatio * right - 0.5f * focalDistance * up;

            UpdateVector();

        }

        public void UpdateVector()
        {
            p1X = new Vector<float>(p1.X);
            p1Y = new Vector<float>(p1.Y);
            p1Z = new Vector<float>(p1.Z);

            p2p1X = Vector.Subtract(new Vector<float>(p2.X), p1X);
            p2p1Y = Vector.Subtract(new Vector<float>(p2.Y), p1Y);
            p2p1Z = Vector.Subtract(new Vector<float>(p2.Z), p1Z);

            p3p1X = Vector.Subtract(new Vector<float>(p3.X), p1X);
            p3p1Y = Vector.Subtract(new Vector<float>(p3.Y), p1Y);
            p3p1Z = Vector.Subtract(new Vector<float>(p3.Z), p1Z);

            posX = new Vector<float>(pos.X);
            posY = new Vector<float>(pos.Y);
            posZ = new Vector<float>(pos.Z);

            upX = new Vector<float>(up.X);
            upY = new Vector<float>(up.Y);
            upZ = new Vector<float>(up.Z);

            rightX = new Vector<float>(right.X);
            rightY = new Vector<float>(right.Y);
            rightZ = new Vector<float>(right.Z);
        }

        public Ray Generate(Random rng, int x, int y)
        {
            float r0 = (float)rng.NextDouble();
            float r1 = (float)rng.NextDouble();
            float r2 = (float)rng.NextDouble() - 0.5f;
            float r3 = (float)rng.NextDouble() - 0.5f;
            // calculate sub-pixel ray target position on screen plane
            float u = ((float)x + r0) / (float)screenWidth;
            float v = ((float)y + r1) / (float)screenHeight;
            Vector3 T = p1 + u * (p2 - p1) + v * (p3 - p1);
            // calculate position on aperture
            Vector3 P = pos + lensSize * (r2 * right + r3 * up);
            // calculate ray direction
            Vector3 D = Vector3.Normalize(T - P);
            // return new primary ray
            return new Ray(P, D, 1e34f);
        }

        public Ray4 GenerateVector(Random rng, int x, int y)
        {
            Ray4 ray = new Ray4();

            Vector<float> r0 = VectorHelp.GenerateRandomVector(rng);
            Vector<float> r1 = VectorHelp.GenerateRandomVector(rng);
            Vector<float> r2 = VectorHelp.GenerateRandomVector(rng, -0.5f);
            Vector<float> r3 = VectorHelp.GenerateRandomVector(rng, -0.5f);

            float[] xx = new float[4] { x, x,     x + 1, x + 1 };
            float[] yy = new float[4] { y, y + 1, y,     y + 1 };

            Vector<float> u = Vector.Divide(Vector.Add(new Vector<float>(xx), r0), screenWidthVector);
            Vector<float> v = Vector.Divide(Vector.Add(new Vector<float>(yy), r1), screenHeightVector);

            Vector<float> Tx = Vector.Add(p1X ,Vector.Add(Vector.Multiply(u, p2p1X), Vector.Multiply(v, p3p1X)));
            Vector<float> Ty = Vector.Add(p1Y, Vector.Add(Vector.Multiply(u, p2p1Y), Vector.Multiply(v, p3p1Y)));
            Vector<float> Tz = Vector.Add(p1Z, Vector.Add(Vector.Multiply(u, p2p1Z), Vector.Multiply(v, p3p1Z)));

            ray.Ox4 = Vector.Add(posX, Vector.Multiply(lensSizeVector, Vector.Add(Vector.Multiply(r2, rightX), Vector.Multiply(r3, upX))));
            ray.Oy4 = Vector.Add(posY, Vector.Multiply(lensSizeVector, Vector.Add(Vector.Multiply(r2, rightY), Vector.Multiply(r3, upY))));
            ray.Oz4 = Vector.Add(posZ, Vector.Multiply(lensSizeVector, Vector.Add(Vector.Multiply(r2, rightZ), Vector.Multiply(r3, upZ))));

            Vector<float> Dx4 = Vector.Subtract(Tx, ray.Ox4);
            Vector<float> Dy4 = Vector.Subtract(Ty, ray.Oy4);
            Vector<float> Dz4 = Vector.Subtract(Tz, ray.Oz4);

            VectorHelp.NormalizeVector(ref Dx4, ref Dy4, ref Dz4);

            ray.Dx4 = Dx4;
            ray.Dy4 = Dy4;
            ray.Dz4 = Dz4;

            ray.t4 = new Vector<float>(1e34f);
            ray.objIdx = new Vector<int>(-1);
            ray.inside = new Vector<int>(0);

            return ray;
        }
}

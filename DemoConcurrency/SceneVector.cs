﻿using System;
using System.Numerics;

using Template;

partial class Scene
{
    /// <summary>
    /// Samples the dome for a ray struct.
    /// </summary>
    public void SampleSkyDomeVector(ref Vector<float> r, ref Vector<float> g, ref Vector<float> b, Vector<float> Dx, Vector<float> Dy, Vector<float> Dz)
    {
        float[] f1 = new float[VectorHelp.instructionSet], f2 = new float[VectorHelp.instructionSet], f3 = new float[VectorHelp.instructionSet];
        for (int j = 0; j < VectorHelp.instructionSet; j++)
        {
            // Math.Atan2(D.X, -D.Z)
            f1[j] = (float)Math.Atan2(Dx[j], -Dz[j]);

            //Math.Acos(D.Y) 
            f2[j] = (float)Math.Acos(Dy[j]);
        }

        Vector<float> aTanVec = new Vector<float>(f1);
        Vector<float> aCosVec = new Vector<float>(f2);

        //int u = (int)(2500.0f * 0.5f * (1.0f + ... * INVPI));
        Vector<float> u = 1250 * (Vector<float>.One + aTanVec * _invpiVector);

        //int v = (int)(1250.0f * ... * INVPI);
        Vector<float> v = 1250 * aCosVec * _invpiVector;
        /*
        int u1 = (int)(1250 * (1.0f + Math.Atan2(Dx[0], -Dz[0]) * INVPI));
        int v1 = (int)(1250.0f * Math.Acos(Dy[0]) * INVPI);
        float at2 = (float)Math.Atan2(Dx[1], -Dz[1]);
        int u2 = (int)(1250 * (1.0f + Math.Atan2(Dx[1], -Dz[1]) * INVPI));
        int v2 = (int)(1250.0f * Math.Acos(Dy[1]) * INVPI);

        float at3 = (float)Math.Atan2(Dx[2], -Dz[2]);
        int u3 = (int)(1250 * (1.0f + Math.Atan2(Dx[2], -Dz[2]) * INVPI));
        int v3 = (int)(1250.0f * Math.Acos(Dy[2]) * INVPI);

        int u4 = (int)(1250 * (1.0f + Math.Atan2(Dx[3], -Dz[3]) * INVPI));
        int v4 = (int)(1250.0f * Math.Acos(Dy[3]) * INVPI);
        */
        // int idx = u + v * 2500;
        Vector<float> idx = (u + v * 2500);

        for (int j = 0; j < VectorHelp.instructionSet; j++)
        {
            f1[j] = skybox[((int)idx[j])];
            f2[j] = skybox[((int)idx[j]) + 1];
            f3[j] = skybox[((int)idx[j]) + 2];
        }

        r = Vector.Add(new Vector<float>(f1), r);
        g = Vector.Add(new Vector<float>(f2), g);
        b = Vector.Add(new Vector<float>(f3), b);
    }

    /// <summary>
    /// Checks whether or not a ray struct is intersecting with the given sphere.
    /// </summary>
    private static void IntersectSphereVector(int idx, Sphere sphere, Ray4 ray)
    {       // Vector3 L = sphere.pos - ray.O
        Vector<float> Lx = Vector.Subtract(new Vector<float>(sphere.pos.X), ray.Ox4);
        Vector<float> Ly = Vector.Subtract(new Vector<float>(sphere.pos.Y), ray.Oy4);
        Vector<float> Lz = Vector.Subtract(new Vector<float>(sphere.pos.Z), ray.Oz4);

        // float tca = Vector3.Dot(L, ray.D)
        Vector<float> tca = VectorHelp.DotVector(Lx, Ly, Lz, ray.Dx4, ray.Dy4, ray.Dz4);

        // if (tca < 0) return
        Vector<int> maskTCA = Vector.LessThan(tca, new Vector<float>(0));
        if (Vector.EqualsAll(maskTCA, VectorHelp.MaskFalse))
            return;

        // float d2 = Vector3.Dot(L, L) - tca * tca
        Vector<float> d2 = Vector.Subtract(VectorHelp.DotVector(Lx, Ly, Lz), Vector.Multiply(tca, tca));

        // if (d2 > sphere.r)
        Vector<int> MaskD2RSub = Vector.GreaterThan(d2, new Vector<float>(sphere.r));
        Vector<int> maskD2R = Vector.BitwiseAnd(MaskD2RSub, maskTCA);
        if (Vector.EqualsAll(MaskD2RSub, VectorHelp.MaskFalse))
            return;

        // float thc = (float)Math.Sqrt(sphere.r - d2)
        Vector<float> thc = Vector.SquareRoot(Vector.Subtract(new Vector<float>(sphere.r), d2));

        // float t0 = tca - thc
        Vector<float> t = Vector.Subtract(tca, thc);

        //if (t0 > 0)
        Vector<int> maskt00 = Vector.GreaterThan(t, new Vector<float>(0));

        if (!Vector.EqualsAll(maskt00, VectorHelp.MaskFalse))
        {

            /*  //-----------------------------------------------------------------------------------------------------------------------
                if (t0 > ray.t) return;
                ray.N = Vector3.Normalize(ray.O + t0 * ray.D - sphere.pos);
                ray.objIdx = idx;
                ray.t = t0;
            */  //-----------------------------------------------------------------------------------------------------------------------

            Vector<int> maskT0rayT = Vector.BitwiseAnd(Vector.GreaterThan(t, ray.t4), maskt00);

            // Vector3.Normalize(ray.O + t0 * ray.D - sphere.pos);
            Vector<float> xN = Vector.Add(ray.Ox4, Vector.Subtract(Vector.Multiply(t, ray.Dx4), new Vector<float>(sphere.pos.X)));
            Vector<float> yN = Vector.Add(ray.Oy4, Vector.Subtract(Vector.Multiply(t, ray.Dy4), new Vector<float>(sphere.pos.Y)));
            Vector<float> zN = Vector.Add(ray.Oz4, Vector.Subtract(Vector.Multiply(t, ray.Dz4), new Vector<float>(sphere.pos.Z)));

            VectorHelp.NormalizeVector(ref xN, ref yN, ref zN);

            // ray.N = ...
            ray.Nx4 = Vector.ConditionalSelect(maskT0rayT, xN, ray.Nx4);
            ray.Ny4 = Vector.ConditionalSelect(maskT0rayT, yN, ray.Ny4);
            ray.Nz4 = Vector.ConditionalSelect(maskT0rayT, zN, ray.Nz4);

            // ray.objIdx = idx;
            ray.objIdx = Vector.ConditionalSelect(maskT0rayT, new Vector<int>(idx), ray.objIdx);

            // ray.t = t0;
            ray.t4 = Vector.ConditionalSelect(maskT0rayT, t, ray.t4);
        }

        /*  //-----------------------------------------------------------------------------------------------------------------------
            if ((t1 > ray.t) || (t1 < 0)) return;
            ray.N = Vector3.Normalize(sphere.pos - (ray.O + t1 * ray.D));
            ray.objIdx = idx;
            ray.t = t1;
        */  //-----------------------------------------------------------------------------------------------------------------------

        // float t1 = tca + thc
        t = Vector.Add(tca, thc);

        Vector<int> maskt1rayTT10D2R = Vector.BitwiseAnd(Vector.BitwiseAnd(Vector.GreaterThan(t, ray.t4), Vector.LessThan(t, new Vector<float>(0))), maskD2R);
        if (Vector.EqualsAll(maskt1rayTT10D2R, VectorHelp.MaskFalse))
            return;

        // Vector3.Normalize(sphere.pos - (ray.O + t1 * ray.D));
        Vector<float> xN2 = Vector.Subtract(Vector.Add(ray.Ox4, Vector.Multiply(t, ray.Dx4)), new Vector<float>(sphere.pos.X));
        Vector<float> yN2 = Vector.Subtract(Vector.Add(ray.Oy4, Vector.Multiply(t, ray.Dy4)), new Vector<float>(sphere.pos.Y));
        Vector<float> zN2 = Vector.Subtract(Vector.Add(ray.Oz4, Vector.Multiply(t, ray.Dz4)), new Vector<float>(sphere.pos.Z));

        VectorHelp.NormalizeVector(ref xN2, ref yN2, ref zN2);

        // ray.N = ...
        ray.Nx4 = Vector.ConditionalSelect(maskt1rayTT10D2R, xN2, ray.Nx4);
        ray.Ny4 = Vector.ConditionalSelect(maskt1rayTT10D2R, yN2, ray.Ny4);
        ray.Nz4 = Vector.ConditionalSelect(maskt1rayTT10D2R, zN2, ray.Nz4);

        // ray.objIdx = idx;
        ray.objIdx = Vector.ConditionalSelect(maskt1rayTT10D2R, new Vector<int>(idx), ray.objIdx);

        // ray.t = t0;
        ray.t4 = Vector.ConditionalSelect(maskt1rayTT10D2R, t, ray.t4);


    }

    /// <summary>
    /// Retrieves the materials that belong to the ray struct.
    /// </summary>
    public MaterialVector GetMaterialVector(Vector<int> objidx, Vector<float> Ix, Vector<float> Iy, Vector<float> Iz)
    {
        MaterialVector mat = new MaterialVector();

        Vector<int> mask;

        /* 
        if (objIdx == 0)
        {
            // procedural checkerboard pattern for floor plane
            mat.refl = mat.refr = 0;
            mat.emissive = false;
            int tx = ((int)(I.X * 3.0f + 1000) + (int)(I.Z * 3.0f + 1000)) & 1;
            mat.diffuse = Vector3.One * ((tx == 1) ? 1.0f : 0.2f);
        }
        */
        mask = Vector.Equals(objidx, VectorHelp.Mask0);

        mat.refl = Vector.ConditionalSelect(mask, new Vector<float>(0), mat.refl);
        mat.emissive = Vector.ConditionalSelect(mask, new Vector<int>(0), mat.emissive);

        // int tx = ((int)(I.X * 3.0f + 1000) + (int)(I.Z * 3.0f + 1000)) & 1;
        Vector<int> tX = Vector.BitwiseAnd(Vector.AsVectorInt32(Vector.Add(Vector.Add(Vector.Multiply(Ix, 3.0f), new Vector<float>(1000)), Vector.Add(Vector.Multiply(Iz, 3.0f), new Vector<float>(1000)))), new Vector<int>(1));

        // mat.diffuse = Vector3.One * ((tx == 1) ? 1.0f : 0.2f);
        Vector<int> mask0 = Vector.Equals(tX, new Vector<int>(1));
        mat.diffuseX = Vector.ConditionalSelect(mask, Vector.Multiply(new Vector<float>(1), Vector.ConditionalSelect(mask0, new Vector<float>(1), new Vector<float>(1.2f))), mat.diffuseX);
        mat.diffuseY = Vector.ConditionalSelect(mask, Vector.Multiply(new Vector<float>(1), Vector.ConditionalSelect(mask0, new Vector<float>(1), new Vector<float>(1.2f))), mat.diffuseY);
        mat.diffuseZ = Vector.ConditionalSelect(mask, Vector.Multiply(new Vector<float>(1), Vector.ConditionalSelect(mask0, new Vector<float>(1), new Vector<float>(1.2f))), mat.diffuseZ);

        /*
        if ((objIdx == 1) || (objIdx > 8)) { mat.refl = mat.refr = 0; mat.emissive = false; mat.diffuse = Vector3.One; }
        */
        mask = Vector.BitwiseAnd(Vector.Equals(objidx, VectorHelp.Mask1), Vector.GreaterThan(objidx, VectorHelp.Mask8));

        mat.refl = Vector.ConditionalSelect(mask, new Vector<float>(0.0f), mat.refl);
        mat.refr = Vector.ConditionalSelect(mask, new Vector<float>(0.0f), mat.refr);
        mat.emissive = Vector.ConditionalSelect(mask, new Vector<int>(0), mat.emissive);

        mat.diffuseX = Vector.ConditionalSelect(mask, new Vector<float>(1.0f), mat.diffuseX);
        mat.diffuseY = Vector.ConditionalSelect(mask, new Vector<float>(1.0f), mat.diffuseY);
        mat.diffuseZ = Vector.ConditionalSelect(mask, new Vector<float>(1.0f), mat.diffuseZ);

        /*
        if (objIdx == 2) { mat.refl = 0.8f; mat.refr = 0; mat.emissive = false; mat.diffuse = new Vector3(1, 0.2f, 0.2f); }
        */

        mask = Vector.Equals(objidx, VectorHelp.Mask2);

        mat.refl = Vector.ConditionalSelect(mask, new Vector<float>(0.8f), mat.refl);
        mat.refr = Vector.ConditionalSelect(mask, new Vector<float>(0.0f), mat.refr);
        mat.emissive = Vector.ConditionalSelect(mask, new Vector<int>(0), mat.emissive);

        mat.diffuseX = Vector.ConditionalSelect(mask, new Vector<float>(1.0f), mat.diffuseX);
        mat.diffuseY = Vector.ConditionalSelect(mask, new Vector<float>(0.2f), mat.diffuseY);
        mat.diffuseZ = Vector.ConditionalSelect(mask, new Vector<float>(0.2f), mat.diffuseZ);

        /*
        if (objIdx == 3) { mat.refl = 0; mat.refr = 1; mat.emissive = false; mat.diffuse = new Vector3(0.9f, 1.0f, 0.9f); }
        */

        mask = Vector.Equals(objidx, VectorHelp.Mask3);

        mat.refl = Vector.ConditionalSelect(mask, new Vector<float>(0.0f), mat.refl);
        mat.refr = Vector.ConditionalSelect(mask, new Vector<float>(1.0f), mat.refr);
        mat.emissive = Vector.ConditionalSelect(mask, new Vector<int>(0), mat.emissive);

        mat.diffuseX = Vector.ConditionalSelect(mask, new Vector<float>(0.9f), mat.diffuseX);
        mat.diffuseY = Vector.ConditionalSelect(mask, new Vector<float>(1.0f), mat.diffuseY);
        mat.diffuseZ = Vector.ConditionalSelect(mask, new Vector<float>(0.9f), mat.diffuseZ);

        /*
        if (objIdx == 4) { mat.refl = 0.8f; mat.refr = 0; mat.emissive = false; mat.diffuse = new Vector3(0.2f, 0.2f, 1); }
        */

        mask = Vector.Equals(objidx, VectorHelp.Mask4);

        mat.refl = Vector.ConditionalSelect(mask, new Vector<float>(0.8f), mat.refl);
        mat.refr = Vector.ConditionalSelect(mask, new Vector<float>(0.0f), mat.refr);
        mat.emissive = Vector.ConditionalSelect(mask, new Vector<int>(0), mat.emissive);

        mat.diffuseX = Vector.ConditionalSelect(mask, new Vector<float>(0.2f), mat.diffuseX);
        mat.diffuseY = Vector.ConditionalSelect(mask, new Vector<float>(0.2f), mat.diffuseY);
        mat.diffuseZ = Vector.ConditionalSelect(mask, new Vector<float>(1.0f), mat.diffuseZ);

        /*
        if (objIdx == 4) { mat.refl = 0.8f; mat.refr = 0; mat.emissive = false; mat.diffuse = new Vector3(0.2f, 0.2f, 1); }
        */

        mask = Vector.Equals(objidx, VectorHelp.Mask8);

        mat.refl = Vector.ConditionalSelect(mask, new Vector<float>(0.0f), mat.refl);
        mat.refr = Vector.ConditionalSelect(mask, new Vector<float>(0.0f), mat.refr);
        mat.emissive = Vector.ConditionalSelect(mask, new Vector<int>(1), mat.emissive);

        mat.diffuseX = Vector.ConditionalSelect(mask, new Vector<float>(8.5f * LIGHTSCALE), mat.diffuseX);
        mat.diffuseY = Vector.ConditionalSelect(mask, new Vector<float>(8.5f * LIGHTSCALE), mat.diffuseY);
        mat.diffuseZ = Vector.ConditionalSelect(mask, new Vector<float>(7.0f * LIGHTSCALE), mat.diffuseZ);

        /*
        if ((objIdx > 4) && (objIdx < 8)) { mat.refl = mat.refr = 0; mat.emissive = false; mat.diffuse = Vector3.One; }
        */

        mask = Vector.BitwiseAnd(Vector.GreaterThan(objidx, VectorHelp.Mask4), Vector.LessThan(objidx, VectorHelp.Mask8));

        mat.refl = Vector.ConditionalSelect(mask, new Vector<float>(0.0f), mat.refl);
        mat.refr = Vector.ConditionalSelect(mask, new Vector<float>(0.0f), mat.refr);
        mat.emissive = Vector.ConditionalSelect(mask, new Vector<int>(0), mat.emissive);

        mat.diffuseX = Vector.ConditionalSelect(mask, new Vector<float>(0.2f), mat.diffuseX);
        mat.diffuseY = Vector.ConditionalSelect(mask, new Vector<float>(0.2f), mat.diffuseY);
        mat.diffuseZ = Vector.ConditionalSelect(mask, new Vector<float>(1.0f), mat.diffuseZ);

        return mat;
    }


    /// <summary>
    /// Checks whether or not the ray struct intersects with the scene.
    /// </summary>
    public static void IntersectVector(Ray4 ray)
    {
        
        IntersectSphereVector(0, plane1, ray);
        IntersectSphereVector(1, plane2, ray);
        for (int i = 0; i < 6; i++)
            IntersectSphereVector(i + 2, sphere[i], ray);
        IntersectSphereVector(8, light, ray);
        
    }
}

﻿using System;
using System.Numerics;

namespace Template
{
    class Sphere
    {
        public Sphere(float x, float y, float z, float radius)
        {
            pos.X = x;
            pos.Y = y;
            pos.Z = z;
            r = radius;
        }
        public Vector3 pos;
        public float r;
    }

} // namespace Template

﻿using System.Numerics;

public class Material
{
    public float refl;
    public float refr;
    public bool emissive;
    public Vector3 diffuse;
}

public class MaterialVector
{
    public Vector<float> refl;
    public Vector<float> refr;
    public Vector<int> emissive;

    public Vector<float> diffuseX, diffuseY, diffuseZ;
}

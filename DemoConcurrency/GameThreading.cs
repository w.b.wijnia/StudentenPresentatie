﻿//#define VECTORISED
//#define DEBUGGING

using System;
using System.Diagnostics;
using System.Threading;
using System.Numerics;

partial class Game
{
    private int THREADS = 1;

    /// <summary>
    /// Allows us to put both the threads as the mainthread to sleep when they are waiting for threads to finish.
    /// </summary>
    EventWaitHandle[] eventThread, eventMain;

    /// <summary>
    /// Our threads.
    /// </summary>
    Thread[] t;

    private const int BLOCKSQUARED = 10;

    int Options = 0;

    private Stopwatch sTest = new Stopwatch();
    private FloatAverage fTest = new FloatAverage(30);

    // 40, 24
    // 40 * 24 = 960 opties

    // 959
    // Y = 19
    // X = 

    /// <summary>
    /// The scale for multiple renders on top of one another.
    /// </summary>
    float scale = 0;

    /// <summary>
    /// Random number generators, one for each thread.
    /// </summary>
    Random[] rng;

    private void InitialiseThreading()
    {   // Retrieve the amount of threads to use.
        int totalThreads = System.Environment.ProcessorCount;
        // Setup the threads and surrounding data.
        rng = new Random[totalThreads];
        eventThread = new EventWaitHandle[totalThreads];
        eventMain = new EventWaitHandle[totalThreads];
        t = new Thread[totalThreads];

        for (int j = 0; j < totalThreads; j++)
        {
            rng[j] = new Random(j);

            eventThread[j] = new EventWaitHandle(false, EventResetMode.ManualReset);
            eventMain[j] = new EventWaitHandle(false, EventResetMode.ManualReset);

            int n = j;
            t[j] = new Thread(() => ThreadWork(n));
            t[j].Name = "Thread number " + n.ToString();
            t[j].Start();
        }
    }

    /// <summary>
    /// The method to call when you want to render the scene in a threaded manner.
    /// </summary>
    private void RenderThreaded()
    {
        // DEMO: check for keyboard input to increase the thread number.
        var keyboard = OpenTK.Input.Keyboard.GetState();

        if (keyboard[OpenTK.Input.Key.Q])
            THREADS += 1;

        if (keyboard[OpenTK.Input.Key.E])
            THREADS -= 1;

        THREADS = Math.Max(1, Math.Min(THREADS, 8));

#if(DEBUGGING)
        Console.WriteLine("\nNew iteration started! \n----------------------");
#endif

        Options = 3680; // 960 * 20 * 20 = 384000
        //Options = 858;

        scale = 1.0f / ++spp;

        for (int j = 0; j < THREADS; j++)
        {   // Tell the threads to go back to work.
            eventThread[j].Set();
            // Tell the main thread (in this case) to go to sleep until all workers are done.
            eventMain[j].Reset();
        }

        // set all threads to go and do the job.
        for (int j = 0; j < THREADS; j++)
            eventMain[j].WaitOne(-1);

        // DEMO
        screen.Print("Threads: " + THREADS.ToString(), 10, 70, 0x000000);

    }

    /// <summary>
    /// The way the threads will render the scene.
    /// </summary>
    /// <param name="i"></param>
    private void ThreadWork(int i)
    {

        while (true)
        {
            // Go to sleep mode until we are required again...
#if (DEBUGGING)
            int rays = 0;
            Stopwatch rayStop = new Stopwatch();
            Stopwatch sampleStop = new Stopwatch();
#endif
            eventThread[i].WaitOne(-1);
            while (true)
            {
                //sTest.Start();
                int o = Interlocked.Decrement(ref Options);
                //sTest.Stop();

                if (o >= 0)
                {
                    int X = o / 46;
                    int Y = o - X * 46;
#if (VECTORISED)
                    for (int y = Y * BLOCKSQUARED, l2 = Math.Min((Y + 1) * BLOCKSQUARED, 440); y < l2; y += 2)
                        for (int x = X * BLOCKSQUARED, l1 = Math.Min((X + 1) * BLOCKSQUARED, 784); x < l1; x += 2)
                        {   // The color of the ray.
                            Vector<float> r = new Vector<float>(0), g = new Vector<float>(0), b = new Vector<float>(0);

                            // Creating and sending the ray.
                            Ray4 ray = camera.GenerateVector(rng[i], x, y);
                            SampleVector(ref r, ref g, ref b, ray, 0, i);

                            // Prepping data
                            int[] pixelIdx = new int[4]
                            {
                                    x + y * screen.width,
                                    (x + 1) + (y + 1) * screen.width,
                                    x + (y + 1) * screen.width,
                                    (x + 1) + y * screen.width
                            };

                            // Sending the data to the screen.
                            for (int j = 0; j < 4; j++)
                            {
                                accumulator[pixelIdx[j]] += new Vector3(r[j], g[j], b[j]);
                                screen.pixels[pixelIdx[j]] = RTTools.Vector3ToIntegerRGB(accumulator[pixelIdx[j]] * scale);
                            }
#if (DEBUGGING)
                            rays += 4;
#endif
                        }
                }

#else
                     for (int y = Y * BLOCKSQUARED, l2 = Math.Min((Y + 1) * BLOCKSQUARED, 440); y < l2; y++)
                        for (int x = X * BLOCKSQUARED, l1 = Math.Min((X + 1) * BLOCKSQUARED, 784); x < l1; x++)
                        {
                            // generate primary ray
#if(DEBUGGING)
                            rayStop.Start();
#endif
                            Ray ray = camera.Generate(rng[i], x, y);
#if(DEBUGGING)
                            rayStop.Stop();
#endif
                            // trace path
                            int pixelIdx = x + y * screen.width;
#if(DEBUGGING)
                            sampleStop.Start();
#endif
                            Vector3 v = SampleIterative(ray, 1, i);
                            //Vector3 v = Sample(ray, 1, i);
                            accumulator[pixelIdx] += v;
#if(DEBUGGING)
                            sampleStop.Stop();
#endif

                            // This is thread safe - every thread is working on their own part of the array.
                            screen.pixels[pixelIdx] = RTTools.Vector3ToIntegerRGB(scale * accumulator[pixelIdx]);
#if (DEBUGGING)
                            rays++;
#endif
                        }
                }
#endif

                else
                {
#if (DEBUGGING)
                    Console.WriteLine(Thread.CurrentThread.Name + " is waiting. Casted " + rays + " rays.");  // Debug

                    string t = "R T: " + (rayStop.ElapsedMilliseconds.ToString("000") + " S T: " + sampleStop.ElapsedMilliseconds.ToString("000") + " t: "+ (rayStop.ElapsedMilliseconds + sampleStop.ElapsedMilliseconds).ToString("000"));
                    screen.Print(t, 10, 90 + 20 * i, 0x000000);
#endif
                    // Tell the main thread we're done.
                    eventMain[i].Set();

                    // Put ourselves (together with the break) into sleep mode . 
                    // Preventing usage of resources on the CPU while waiting for the next iteration.
                    eventThread[i].Reset();
                    break;
                }
            }
        }
    }
}
